#
# Cookbook Name:: stack-builder
# Recipe:: default
#
# Copyright (C) 2016 Eric Lee Stewart
#
# MIT License
#

# Install apt.
include_recipe 'apt'

# Update local packages.
execute 'update_packages' do
  command 'apt-get update'
  user 'root'
end

# Install packages if necessary.
if node[:config_json] && node[:config_json][:packages]
  node[:config_json][:packages].each do | package_name |
    package package_name do
      action :install
    end
  end
end

# Setup mysql configs.
if node[:config_json] && node[:config_json][:mysql] && node[:config_json][:mysql][:username]
  conf_mysql_username = node[:config_json][:mysql][:username]
else
  conf_mysql_username = "root"
end

if node[:config_json] && node[:config_json][:mysql] && node[:config_json][:mysql][:password]
  conf_mysql_password = node[:config_json][:mysql][:password]
else
  conf_mysql_password = "root"
end

if node[:config_json] && node[:config_json][:mysql] && node[:config_json][:mysql][:database]
  conf_mysql_database = node[:config_json][:mysql][:database]
else
  conf_mysql_database = "project_local"
end

# Install MySQL
mysql_service 'default' do
  charset 'utf8'
  port '3306'
  version '5.5'
  initial_root_password conf_mysql_password
  action [:create, :start]
end

# Install mysql chef gem.
mysql2_chef_gem 'default' do
  action :install
end

# Create the database.
mysql_database conf_mysql_database do
 connection(
   :host     => '127.0.0.1',
   :username => conf_mysql_username,
   :password => conf_mysql_password
 )
 action :create
end

# Create user root@% with all priviledges except GRANT.
mysql_database_user conf_mysql_username do
  connection(
    :host     => '127.0.0.1',
    :username => conf_mysql_username,
    :password => conf_mysql_password
  )
  password conf_mysql_password
  host       '%'
  action     :grant
end

# Install php-fpm
package "php5-fpm" do
  action :install
end

# Install php extensions.
if node[:config_json] && node[:config_json][:php] && node[:config_json][:php][:extensions]
  node[:config_json][:php][:extensions].each do | package_name |
    package package_name do
      action :install
    end
  end
end

# Update /etc/php5/cli/php.ini
template "/etc/php5/cli/php.ini" do
  path "/etc/php5/cli/php.ini"
  source "php5/cli/php.ini.erb"
  mode 0644
end

# Update /etc/php5/fpm/pool.d/www.conf
template "/etc/php5/fpm/pool.d/www.conf" do
  path "/etc/php5/fpm/pool.d/www.conf"
  source "php5/fpm/pool.d/www.conf.erb"
  mode 0644
end

# Update /etc/php5/fpm/php-fpm.conf
template "/etc/php5/fpm/php-fpm.conf" do
  path "/etc/php5/fpm/php-fpm.conf"
  source "php5/fpm/php-fpm.conf.erb"
  mode 0644
end

# Update /etc/php5/fpm/php.ini
template "/etc/php5/fpm/php.ini" do
  path "/etc/php5/fpm/php.ini"
  source "php5/fpm/php.ini.erb"
  mode 0644
end

# Install nginx
package "nginx" do
  action :install
end

# Update /etc/nginx/nginx.conf
template "/etc/nginx/nginx.conf" do
  path "/etc/nginx/nginx.conf"
  source "nginx/nginx.conf.erb"
  mode 0644
end

# Update /etc/nginx/sites-available/default
template "/etc/nginx/sites-available/default" do
  path "/etc/nginx/sites-available/default"
  source "nginx/sites-available/default.erb"
  mode 0644
  helpers do

    def http_port
      if node[:config_json] && node[:config_json][:nginx] && node[:config_json][:nginx][:http_port]
        value = node[:config_json][:nginx][:http_port]
      else
        value = 80
      end
      "#{value}"
    end

    def rewrites
      value = ""
      if node[:config_json] && node[:config_json][:nginx] && node[:config_json][:nginx][:rewrites]
        if node[:config_json][:nginx][:rewrites] == "laravel"
          value = "location / {\n"
          value += "        try_files $uri $uri/ /index.php?$query_string;\n"
          value += "    }"
        end
      end
      "#{value}"
    end

    def root_folder
      if node[:config_json] && node[:config_json][:nginx] && node[:config_json][:nginx][:root_folder]
        value = "/" + node[:config_json][:nginx][:root_folder]
      else
        value = ""
      end
      "#{value}"
    end
  end
end

# Enable and start php-fpm.
service 'php5-fpm' do
  action [ :enable, :start, :restart ]
end

# Enable and start nginx.
service 'nginx' do
  action [ :enable, :start, :restart ]
end
