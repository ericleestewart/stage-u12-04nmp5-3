name             'stack-builder'
maintainer       'Eric Lee Stewart'
maintainer_email 'e.stewart@mac.com'
license          'MIT License'
description      'Installs/Configures stack-builder'
long_description 'Installs/Configures stack-builder'
version          '0.1.0'

depends          'apt'
depends          'mysql', '~> 7.2.0'
depends          'database'
depends          'mysql2_chef_gem'
