# -*- mode: ruby -*-
# vi: set ft=ruby :
require 'json'

# Load external configuration
if File.file?("conf-local.json")
  ext_conf = JSON.load(File.new("conf-local.json"))
elsif File.file?("conf-defaults.json")
  ext_conf = JSON.load(File.new("conf-defaults.json"))
end

Vagrant.configure("2") do |config|
  # Box - Ubuntu 12.04
  config.vm.box = "bento/ubuntu-12.04"
  # Network
  config.vm.network "forwarded_port", guest: ext_conf["nginx"]["http_port"], host: ext_conf["host_machine"]["http_port"]
  config.vm.network "forwarded_port", guest: ext_conf["mysql"]["port"], host: ext_conf["host_machine"]["mysql_port"]
  # Shared Folders
  config.vm.synced_folder ext_conf["host_machine"]["app_src"], "/var/www"
  # Chef Integration
  config.omnibus.chef_version = :latest
  config.vm.provision "chef_solo" do |chef|
    # Chef cookbook location.
    chef.cookbooks_path = "./chef/cookbooks"
    # Chef roles location.
    chef.roles_path = "./chef/roles"
    # Chef data bag location.
    chef.data_bags_path = "./chef/data-bags"
    # Setup role to run.
    chef.add_role "stack-builder"
    # Set configuration.
    chef.json = { "config_json" => ext_conf }
  end
  # VirtualVBox Settings
  config.vm.provider "virtualbox" do |vb|
    # VM Name
    vb.name = ext_conf["vm"]["name"]
    # Don't display VirtualBox GUI on vagrant up.
    vb.gui = false
    # VM memory
    vb.memory = ext_conf["vm"]["memory"]
    # Set network adaptor to fix VB issue with MacOS X.
    vb.customize ["modifyvm", :id, "--nictype1", "Am79C973"]
  end
end

