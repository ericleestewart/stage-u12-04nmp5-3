PHP Development Environment
===========================

Environment Software
--------------------

+ Ubuntu 12.04
+ MySQL 5.5.49
+ Nginx 1.1.19
+ PHP 5.3.10
+ PHP-FPM 5.3.10

Host Machine Requirements
-------------------------

+ VirtualBox
+ Vagrant

Environment Setup Instructions
------------------------------

1. Clone the repository: 
https://ericleestewart@bitbucfket.org/ericleestewart/stage-u12-04nmp5-3.git

2. Duplicate conf-defaults.json to make conf-local.json. Edit settings to
configure the environment you need.

3. Create the vm with the following command:
```
vagrant up
```

Environment Development Notes:
------------------------------

Cookbook mysql 8.0.0 has a bug that causes mysql to fail while booting. I
installed version 7.2.0 (the version right before 8.0.0) and it works.

Run the following to log into MySQL locally:
```
mysql -h 127.0.0.1 -u root -p
```
